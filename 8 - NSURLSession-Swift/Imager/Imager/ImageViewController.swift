//
//  ImageViewController.swift
//  Imager
//
//  Created by Michael Ward on 7/22/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var imageView: UIImageView!
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        fetchImage(textField.text ?? "")
        return true
    }

    func fetchImage(_ urlString: String) {
        guard let url = URL(string: urlString) else {
            alertFailure("That doesn't seem to be a valid image URL.")
            return
        }
        
        let request = URLRequest(url: url)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {
            (data: Data?, response: URLResponse?, error: Error?) in
            guard let data = data, let _ = response as? HTTPURLResponse else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.alertFailure("I asked for the image but the server hung up.")
                })
                return
            }
            
            DispatchQueue.main.async(execute: {
                let image = UIImage(data: data)
                self.imageView.image = image
            })
        })
        
        task.resume()
    }
    
    func alertFailure(_ message: String) {
        let alertController = UIAlertController(
            title: "Download Failed",
            message: message,
            preferredStyle: UIAlertControllerStyle.alert )
        let dismissAction = UIAlertAction(title: "Oh Well", style: UIAlertActionStyle.default) {
            (action:UIAlertAction!)->() in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(dismissAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

