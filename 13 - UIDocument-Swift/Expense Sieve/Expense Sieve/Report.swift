//
//  Report.swift
//  Expense Sieve
//
//  Created by Michael Ward on 7/7/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class Report: NSObject, NSCoding {
    
    // MARK: - Basic state
    
    private(set) var identifier = UUID().uuidString
    private(set) var creationDate = Date()
    var title: String?
    var expenses: [Expense] = []
    var expenseTotal: Double {
        return expenses.map({$0.amount}).reduce(0.0,+)
    }
    var summary: Summary {
        return Summary(report: self)
    }
    
    override init() {
        super.init()
    }
    
    // MARK: - NSCoding
    
    private enum CoderKey {
        static let identifier = "identifier"
        static let creationDate = "creationDate"
        static let title = "title"
        static let expenses = "expenses"
    }
    
    required init?(coder aDecoder: NSCoder) {
        title = aDecoder.decodeObject(forKey: CoderKey.title) as? String
        
        guard let identifier = aDecoder.decodeObject(forKey: CoderKey.identifier) as? String,
            let creationDate = aDecoder.decodeObject(forKey: CoderKey.creationDate) as? Date,
            let expenses = aDecoder.decodeObject(forKey: CoderKey.expenses) as? [Expense] else {
                return nil
        }
        
        self.identifier = identifier
        self.creationDate = creationDate
        self.expenses = expenses
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(identifier, forKey: CoderKey.identifier)
        aCoder.encode(creationDate, forKey: CoderKey.creationDate)
        aCoder.encode(title, forKey: CoderKey.title)
        aCoder.encode(expenses, forKey: CoderKey.expenses)
    }
    
    // MARK: - Summary
    
    struct Summary {
        let identifier: String
        let title: String?
        let expenseTotal: Double
        
        init(report: Report) {
            identifier = report.identifier
            title = report.title
            expenseTotal = report.expenseTotal
        }
    }
}
