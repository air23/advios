//
//  Expense.swift
//  Expense Sieve
//
//  Created by Michael Ward on 7/7/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import Foundation

class Expense: NSObject, NSCoding {
    
    private(set) var photoKey = UUID().uuidString
    var amount = 0.0
    var vendor: String?
    var comment: String?
    var date = Date()
    
    override init() {
        super.init()
    }
    
    // MARK: - NSCoding
    
    private enum CoderKey {
        static let photoKey = "photoKey"
        static let amount = "amount"
        static let vendor = "vendor"
        static let comment = "comment"
        static let date = "date"
    }
    
    required init?(coder aDecoder: NSCoder) {
        amount = aDecoder.decodeDouble(forKey: CoderKey.amount)
        vendor = aDecoder.decodeObject(forKey: CoderKey.vendor) as? String
        comment = aDecoder.decodeObject(forKey: CoderKey.comment) as? String
        
        guard let photoKey = aDecoder.decodeObject(forKey: CoderKey.photoKey) as? String,
              let date = aDecoder.decodeObject(forKey: CoderKey.date) as? Date else {
            return nil
        }

        self.photoKey = photoKey
        self.date = date
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(photoKey, forKey: CoderKey.photoKey)
        aCoder.encode(amount, forKey: CoderKey.amount)
        aCoder.encode(vendor, forKey: CoderKey.vendor)
        aCoder.encode(comment, forKey: CoderKey.comment)
        aCoder.encode(date, forKey: CoderKey.date)
    }
}
