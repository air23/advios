import UIKit

class ProductDetailViewController : UIViewController {
    @IBOutlet var materialLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var blurbTextView: UITextView!
    @IBOutlet var specsTextView: UITextView!
    
    var store: CatalogStore!
    var product: Product!
    
    
    private func updateUI() {
        materialLabel.text = product.name
        let price = Double(product.price) / 100.0
        priceLabel.text = "\(price)"
        
        if let blurbs = product.blurbs {
            blurbTextView.text = (blurbs.joined(separator: "\n"))
        } else {
            blurbTextView.text = ""
        }
        
        if let specs = product.specs {
            specsTextView.text = (specs.joined(separator: "\n"))
        } else {
            specsTextView.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        store.fetchProductDetail(product) {
            productResult in
            
            switch productResult {
            case .success:
                self.updateUI()
            case let .failure(error):
                print("Error fetching products: \(error)")
            }
        }
    }
}
