//
//  main.m
//  Messagr
//
//  Created by Michael L Ward on 6/21/13.
//  Copyright (c) 2013 Big Nerd Ranch. All rights reserved.
//


@import Foundation;
@import ObjectiveC.message;

int main (int argc, const char * argv[])
{
	@autoreleasepool {
		NSString *name = @"Mikey Ward";
        NSString *capsName = objc_msgSend(name, @selector(uppercaseString));
		NSLog(@"%@ -> %@",name,capsName);
	}
	return 0;
}