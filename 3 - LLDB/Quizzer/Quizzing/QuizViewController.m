#import "QuizViewController.h"

#import "QuizItem.h"
#import "QuizSet.h"
#import "QuizEndViewController.h"

static const NSTimeInterval kAnimationTime = 0.75;

@interface QuizViewController ()

@property (assign, nonatomic) NSUInteger currentQuestion;
@property (assign, nonatomic) NSUInteger currentScore;

@property (weak, nonatomic) IBOutlet UILabel *quizNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end // extension


@implementation QuizViewController

- (id) initWithNibName: (NSString *) nibNameOrNil  bundle: (NSBundle *) nibBundleOrNil {
    self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil];
    if (self)  {
    }
    return self;
} // initWithNibName


- (NSString *) questionNumberTextWithCurrentQuestion: (NSUInteger) currentQuestion {
    NSString *text = [NSString stringWithFormat: @"%d / %d",
                               currentQuestion, self.quizSet.items.count];
    return text;
} // questionNumberTextWithCurrentQuestion


- (NSString *) scoreTextWithScore: (NSUInteger) score {
    NSString *text = [NSString stringWithFormat: @"Score: %d", score];
    return text;
} // socreTextWithScore


- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = self.quizSet.title;

    self.quizNameLabel.text = self.quizSet.title;
    self.questionLabel.text = @"";
    self.answerTextField.text = @"";
    self.imageView.image = nil;
    self.questionNumberLabel.text = [self questionNumberTextWithCurrentQuestion: 0];
    self.scoreLabel.text = [self scoreTextWithScore: 0];

} // viewDidLoad


- (void) viewDidAppear: (BOOL) animated {
    [self moveToQuestion: 0];
} // viewDidAppear


- (void) moveToQuestion: (NSUInteger) question {
    if (question >= self.quizSet.items.count) {
        [self finishedQuiz];
        return;
    }

    self.currentQuestion = question;
    [self updateUI];
} // moveToQuestion


- (void) finishedQuiz {
    QuizEndViewController *qevc = [QuizEndViewController new];
    qevc.quizSet = self.quizSet;
    qevc.score = self.currentScore;
    qevc.oldHighScore = self.quizSet.highScore;

    [self.navigationController pushViewController: qevc  animated: YES];

    if (self.currentScore > self.quizSet.highScore) {
        self.quizSet.highScore = self.currentScore;
    }

} // finishedQuiz


- (void) scoreAndMoveOn {
    QuizItem *item = self.currentItem;
    BOOL correct = NO;
    NSString *answer = self.answerTextField.text;
    if ([answer compare: item.answer
                options: NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch
                | NSWidthInsensitiveSearch] == NSOrderedSame) {
        correct = YES;
    }

    if (correct) {
        self.currentScore++;
        [self expressHappiness];
    } else {
        [self expressSadness];
    }

} // scoreAndMoveOn


- (QuizItem *) currentItem {
    QuizItem *item = self.quizSet.items[self.currentQuestion];
    return item;

} // currentItem


- (void) updateUI {
    QuizItem *item = self.currentItem;
    
    self.questionLabel.text = item.question;
    self.answerTextField.text = @"";
    UIImage *image = [UIImage imageNamed: item.imageName];
    self.imageView.image = image;
    self.questionNumberLabel.text = [self questionNumberTextWithCurrentQuestion: self.currentQuestion + 1];
    self.scoreLabel.text = [self scoreTextWithScore: self.currentScore];
    
} // updateUI


- (void) expressHappiness {
    [self expressImageNamed: @"happy"  label: nil];
} // expressHappiness


- (void) expressSadness {
    QuizItem *item = self.currentItem;
    [self expressImageNamed: @"frowny"  label: item.answer];
} // expressSadness


- (void) expressImageNamed: (NSString *) imageName
                     label: (NSString *) labelText {
    UIImage *image = [UIImage imageNamed: imageName];
    assert(image);

    CGRect frame = self.view.bounds;
    frame = CGRectInset (frame, frame.size.width * 0.25, frame.size.width * 0.25);

    UIImageView *imageView = [[UIImageView alloc] initWithFrame: frame];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = image;
    imageView.alpha = 0.0;
    [self.view addSubview: imageView];

    UILabel *label;
    if (labelText) {
        label = [[UILabel alloc] initWithFrame: CGRectZero];
        label.text = labelText;
        label.alpha = 0.0;
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor yellowColor];
        UIFont *font = [UIFont boldSystemFontOfSize: 30];
        label.font = font;
        
        [label sizeToFit];

        CGRect labelFrame = label.frame;
        labelFrame.size.width = self.view.frame.size.width;
        labelFrame.origin.x = 0.0;
        labelFrame.origin.y = CGRectGetMaxY (frame);
        label.frame = labelFrame;

        [self.view addSubview: label];
    }

    [UIView animateWithDuration: kAnimationTime
            animations: ^{
                 imageView.alpha = 1.0;
                 label.alpha = 1.0;
            }
            completion: ^(BOOL finished) {
                 imageView.alpha = 1.0;
                 label.alpha = 1.0;

                 [UIView animateWithDuration: kAnimationTime
                         animations: ^{
                             imageView.alpha = 0.0;
                             label.alpha = 0.0;
                         }
                         completion: ^ (BOOL finished) {
                             [imageView removeFromSuperview];
                             [label removeFromSuperview];
                             [self moveToQuestion: self.currentQuestion + 1];
                         }];
        }];

} // expressImageNamed


- (IBAction) dismissKeyboard {
    [self.view endEditing: YES];
} // dismissKeyboard


- (BOOL) textFieldShouldReturn: (UITextField *) textField {
    [self dismissKeyboard];
    [self scoreAndMoveOn];
    return YES;
} // textFieldShouldReturn



@end // BNRQuizViewController

