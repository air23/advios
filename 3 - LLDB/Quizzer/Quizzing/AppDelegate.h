//
//  BNRAppDelegate.h
//  Quizzing
//
//  Created by Mark Dalrymple on 3/31/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
