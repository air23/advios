#import <UIKit/UIKit.h>

@class QuizSet;

@interface QuizViewController : UIViewController

@property (strong, nonatomic) QuizSet *quizSet;

@end // BNRQuizViewController

