#import <Foundation/Foundation.h>

@interface QuizItem : NSObject
@property (copy, nonatomic) NSString *question;
@property (copy, nonatomic) NSString *answer;
@property (copy, nonatomic) NSString *imageName;
@end // BNRQuizItem
