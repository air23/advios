#import <UIKit/UIKit.h>

@interface QuizMenuViewController : UIViewController

@property (copy, nonatomic) NSArray *quizzes;  // Array of BNRQuizSets

@end // BNRQuizMenuViewController

