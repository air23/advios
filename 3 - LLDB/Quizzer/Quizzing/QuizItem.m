#import "QuizItem.h"

@implementation QuizItem



- (NSString *) truncateString: (NSString *) string
             toCharacterCount: (NSUInteger) count {
    NSRange range = { 0, MIN(string.length, count) };
    range = [string rangeOfComposedCharacterSequencesForRange: range];
    NSString *trunc = [string substringWithRange: range];

    if (trunc.length < string.length) {
        trunc = [trunc stringByAppendingString: @"..."];
    }

    return trunc;

} // truncateString


- (NSString *) description {
    NSString *question = [self truncateString: self.question  toCharacterCount: 20];
    NSString *answer = [self truncateString: self.answer  toCharacterCount: 10];
    NSString *imageName = [self truncateString: self.imageName  toCharacterCount: 20];
    
    NSString *description = [NSString stringWithFormat: @"<%@: %p> %@ - %@ - %@",
                                      self.class, self, question, answer, imageName];
    return description;
} // description

@end // BNRQuizItem
