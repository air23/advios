#import "QuizEndViewController.h"

#import "QuizSet.h"

@interface QuizEndViewController ()

@property (weak, nonatomic) IBOutlet UILabel *finalScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *highScoreLabel;
@end // extension



@implementation QuizEndViewController

- (id) initWithNibName: (NSString *) nibNameOrNil  bundle: (NSBundle *) nibBundleOrNil
{
    self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil];

    if (self)  {
    }

    return self;

} // initWithNibName


- (void) viewDidLoad {
    [super viewDidLoad];

    if (self.score == self.quizSet.items.count) {
        self.finalScoreLabel.text = @"Perfect Score!";
    } else {
        NSString *outOf = [NSString stringWithFormat: @"Final Score: %d of out %d",
                                    self.score, self.quizSet.items.count];
        self.finalScoreLabel.text = outOf;
    }

    if (self.score > self.oldHighScore) {
        self.highScoreLabel.text = @"New High Score!";

    } else if (self.score != 0 && self.score == self.oldHighScore) {
        self.highScoreLabel.text = @"You Tied the High Score!";
    } else {
        NSString *currentScore = [NSString stringWithFormat: @"High Score: %d",
                                           self.oldHighScore];
        self.highScoreLabel.text = currentScore;
    }
    
} // viewDidLoad


- (IBAction) dismiss {
    [self.navigationController popToRootViewControllerAnimated: YES];
} // dismiss


@end // BNRQuizEndViewController
