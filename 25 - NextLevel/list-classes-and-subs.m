#import <Foundation/Foundation.h>
#import <objc/runtime.h>

// clang -g -Wall -framework Foundation -o list-classes-and-subs list-classes-and-subs.m

static void ListAllClasses (void) {
    unsigned int classCount;
    Class *classes = objc_copyClassList (&classCount);

    qsort_b (classes, classCount, sizeof(Class),
             ^(const void *thing1, const void *thing2) {
       return strcmp(class_getName(*((Class *)thing1)),
                     class_getName(*((Class *)thing2)));
    });


    for (int i = 0; i < classCount; i++) {
        Class clas = classes[i];
        printf ("%s\n", class_getName(clas));
    }

    free (classes);

} // ListAllClasses


static void ListAllSubclassesOf (Class superclass) {

    printf ("Looking for subclasses of %s\n", class_getName(superclass));

    unsigned int classCount;
    Class *classes = objc_copyClassList (&classCount);

    qsort_b (classes, classCount, sizeof(Class),
             ^(const void *thing1, const void *thing2) {
            return strcmp(class_getName(*((Class *)thing1)),
                          class_getName(*((Class *)thing2)));
        });

    for (int i = 0; i < classCount; i++) {
        Class clas = classes[i];

        // Filter out problems.
        Class immediateSuperclass = class_getSuperclass (clas);

        if (immediateSuperclass == NULL) continue;

        if (strcmp(class_getName(immediateSuperclass), "Object") == 0) {
            // "Protocol" inherits from Object (not NSObject), and so
            // doesn't respond to isSubclassOfClass
            continue;
        }

        if ([clas isSubclassOfClass: superclass]) {
            printf ("    %s\n", class_getName(clas));
        }
    }

    free (classes);

} // ListAllSubclassesOf


int main (void) {

    @autoreleasepool {
        ListAllClasses ();
        
        printf ("--------------------------------------------------\n");
        
        ListAllSubclassesOf ([NSString class]);
        ListAllSubclassesOf ([NSValue class]);
    }
    return 0;

} // main

