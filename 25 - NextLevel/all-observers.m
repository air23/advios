#import <Foundation/Foundation.h>

static NSMutableDictionary *g_observers;

@interface NSNotificationCenter (BNRSpying)
@end // observer tracking


@implementation NSNotificationCenter (BNRSpying)

+ (void) load {
    g_observers = [NSMutableDictionary dictionary];

    SwizzleMethod (self.class, @selector(removeObserver:),
                   @selector($bnr_notificationcenter_removeObserver));

    SwizzleMethod (self.class, @selector(removeObserver:name:object:),
                   @selector(bnr_notificationcenter_removeObserver:name:object:));

    SwizzleMethod (self.class, @selector(addObserverForName:object:queue:usingBlock:),
                   @selector(bnr_notificationcenter_addObserverForName:object:queue:usingBlock:));

    SwizzleMethod (self.class, @selector(addObserver:selector:name:object:),
                   @selector(bnr_notificationcenter_addObserver:selector:name:object:));

    NSLog (@"robbery!");
} // load


#define WRAP(x) [NSValue valueWithNonretainedObject:(x)]


- (void) bnr_notificationcenter_removeObserver: (id) observer {
    NSLog (@"%s", __func__);
    [self bnr_notificationcenter_removeObserver: observer];

    @synchronized (g_observers) {
        [g_observers removeObjectForKey: WRAP(observer)];
    }
        
} // removeObserver

- (void) bnr_notificationcenter_removeObserver: (id) observer
                                          name: (NSString *) aName
                                        object: (id) anObject {
    NSLog (@"%s", __func__);
    [self bnr_notificationcenter_removeObserver: observer
          name: aName
          object: anObject];

    @synchronized (g_observers) {
        [g_observers removeObjectForKey: WRAP(observer)];
    }
        
} // removeObserver

- (id) bnr_notificationcenter_addObserverForName: (NSString *) name
                                          object: (id) obj
                                           queue: (NSOperationQueue *) queue
                                      usingBlock: (void (^) (NSNotification *note) ) block {
    NSLog (@"%s", __func__);
    id result = [self bnr_notificationcenter_addObserverForName: name
                      object: obj
                      queue: queue
                      usingBlock: block];
    name = name ?: @"<all the things>";

    @synchronized (g_observers) {
        [g_observers setObject: @{ @"name" : name, @"selector" : @"block" }
        forKey: WRAP(result)];
    }

    return result;
} // addObserverForName


- (void) bnr_notificationcenter_addObserver: (id) observer 
                                   selector: (SEL) selector
                                       name: (NSString *) name
                                     object: (id) object {
    NSLog (@"%s", __func__);
    [self bnr_notificationcenter_addObserver: observer
          selector: selector
          name: name
          object: object];

    name = name ?: @"<all the things>";

    NSDictionary *info =
        @{ @"name" : name, 
           @"selector" : NSStringFromSelector(selector),
           @"class" : NSStringFromClass([observer class])
    };


    @synchronized (g_observers) {
        [g_observers setObject: info
                     forKey: WRAP(observer)];
    }

} // addObserver

@end // observer tracking


