#import "BNRNotificationSpy.h"

@implementation BNRNotificationSpy

// turn on listening for all notifications.
+ (void) startSpying {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver: self
            selector: @selector(observeDefaultCenterStuff:)
            name: nil
            object: nil];
}


// remove us as observers
+ (void) stopSpying {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver: self];
}


+ (void) observeDefaultCenterStuff: (NSNotification *) notification {
    NSArray *notificationsToSquelch = @[
         @"NSUserDefaults",
         @"UIScreenBrightnessDidChange",
         @"UITextSelectionDidScroll",
         @"UITextSelectionWillScroll",
         @"UIViewAnimationDid",
         @"WAKView",
         @"WebPreferences",
         @"WebViewDidChange",
         @"_UIApplicationSystemGestureStateChangedNotification",
         @"_UIScrollViewAnimationEndedNotification"
         @"UIViewAnimationDid",
    ];

    notificationsToSquelch = nil;
    
    for (NSString *name in notificationsToSquelch) {
        if ([notification.name hasPrefix:name]) return;
    }
    
    id userInfo = notification.userInfo ?: @"(null)";
    
    NSLog(@"Observed %@ : %@", notification.name, userInfo);
}


@end // BNRNotificationSpy



