#import <objc/runtime.h>

static void SwizzleMethod (Class clas, SEL originalSelector, SEL newSelector) {

    Method originalMethod =
        class_getInstanceMethod (clas, originalSelector);
    Method newMethod =
        class_getInstanceMethod (clas, newSelector);
    
    BOOL addedMethod =
        class_addMethod (clas, originalSelector,
                         method_getImplementation(newMethod),
                         method_getTypeEncoding(newMethod));
    if (addedMethod) {
        class_replaceMethod (clas, newSelector,
                             method_getImplementation(originalMethod),
                             method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations (originalMethod, newMethod);
    }

} // SwizzleMethod

