To The Next Level
=================

Interesting stuff in here:

* `spy.d` -- the spying DTrace script
* `*.webloc` -- some bookmarks for things like class-dump and the objective-C runtime API
* `BNRNotificationSpy.[hm]` -- drop-in class to spy on posted notifications
* `swizzle.m` -- SwizzleMethod function
* `list-classes-and-subs.m` -- Mac command-line program to list all classes, and list subclasses
* `all-observers.m` -- the grand finale, seeing all registered observers.