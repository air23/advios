//
//  SlideTransition.swift
//  WorldTrotter
//
//  Created by Michael Ward on 5/2/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

enum SlideDirection {
    case fromLeft
    case fromRight
}

class SlideTransition: NSObject, UIViewControllerAnimatedTransitioning {
    let animationDuration: TimeInterval = 0.35
    var animationDirection: SlideDirection = .fromRight
    
    func transitionDuration(using transitionContext:
            UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        // Grab the incoming, outgoing, and parent views from the context
        let container = transitionContext.containerView
        guard let toView = transitionContext.view(forKey: .to),
            let fromView = transitionContext.view(forKey: .from) else {
                preconditionFailure("Transition started with missing context info!")
        }
        
        // Add the toView to the view hierarchy (the fromView is already there)
        container.insertSubview(toView, at: 0)
        
        // Take snapshots of both views for the animation
        guard let toSnap = toView.snapshotView(afterScreenUpdates: true),
            let fromSnap = fromView.snapshotView(afterScreenUpdates: true) else {
                return
            }
        
        // Position the outgoing snapshot where the outgoing view is now
        fromSnap.frame = fromView.frame
        
        // Position the incoming snapshot just offscreen on the correct side
        switch animationDirection {
        case .fromRight:
            toSnap.frame.origin.x = fromView.frame.origin.x - toView.frame.width
        case .fromLeft:
            toSnap.frame.origin.x = fromView.frame.origin.x + fromView.frame.width
        }
        
        // Add the inset for fun visual "depth"
        toSnap.frame = toSnap.frame.insetBy(dx: 20.0, dy: 40.0)
        
        // Replace the top-level views with their snapshots for the animation
        container.addSubview(toSnap)
        container.addSubview(fromSnap)
        toView.removeFromSuperview()
        fromView.removeFromSuperview()
        
        // Define an inner function that will actually perform the animation when called
        func updateFrames() {
            // Move the incoming snapshot to where the outgoing snapshot is now
            toSnap.frame = fromSnap.frame
            
            // How far (and in which direction) should the outgoing snapshot move?
            var dX = fromSnap.frame.width
            if self.animationDirection == .fromLeft {
                dX *= -1.0
            }

            // Move the outgoing snapshot offscreen
            fromSnap.frame.origin.x += dX
            fromSnap.frame = fromSnap.frame.insetBy(dx: 20.0, dy: 40.0)
        }
        
        // Define another inner function to make the necessary view hierarchy changes
        func cleanUp(_ didComplete: Bool) {
            container.addSubview(toView)
            toSnap.removeFromSuperview()
            fromSnap.removeFromSuperview()
            transitionContext.completeTransition(true)
        }
        
        // Kick off the animation
        UIView.animate(withDuration: animationDuration, animations: updateFrames, completion: cleanUp)
    }
}
