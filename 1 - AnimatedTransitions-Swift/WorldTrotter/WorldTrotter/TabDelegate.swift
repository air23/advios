//
//  TabDelegate.swift
//  WorldTrotter
//
//  Created by Michael Ward on 5/2/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class TabDelegate: NSObject, UITabBarControllerDelegate {
    
    private let slideTransition = SlideTransition()

    func tabBarController(_ tabBarController: UITabBarController,
        animationControllerForTransitionFrom fromVC: UIViewController,
        to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        slideTransition.animationDirection =
            animationDirectionInTabBarController(tabBarController, from: fromVC, to: toVC)
        return slideTransition
    }
    
    func animationDirectionInTabBarController(_ tabBarController: UITabBarController,
                                              from: UIViewController,
                                              to: UIViewController) -> SlideDirection {

        let tabControllers = tabBarController.viewControllers!
        precondition(tabControllers.contains(from))
        precondition(tabControllers.contains(to))
        
        guard let fromIndex = tabControllers.index(of: from),
            let toIndex = tabControllers.index(of: to) else {
                return .fromLeft
            }
        
        if fromIndex < toIndex {
            return .fromLeft
        } else {
            return .fromRight
        }
    }
    
}
