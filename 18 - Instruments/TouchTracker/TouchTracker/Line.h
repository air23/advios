//
//  Line.h
//  TouchTracker
//
//  Created by Nate Chandler on 11/29/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Line : NSObject

@property (nonatomic) CGPoint begin;
@property (nonatomic) CGPoint end;

- (instancetype)initWithBegin:(CGPoint)begin end:(CGPoint)end;

@end
