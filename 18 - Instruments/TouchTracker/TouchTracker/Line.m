//
//  Line.m
//  TouchTracker
//
//  Created by Nate Chandler on 11/29/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import "Line.h"

@implementation Line

- (instancetype)initWithBegin:(CGPoint)begin end:(CGPoint)end
{
    self = [super init];
    if (self) {
        _begin = begin;
        _end = end;
    }
    return self;
}

@end
