//
//  ReportViewController.swift
//  SalesReport
//
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {

    // DEMO 1 - used `ReportLayer`, removed `reportRenderer`
    private var reportLayer: ReportLayer!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up people
        var fred = Person(name: "Fred", imageNamed: "fred")
        fred.sales = 134

        var matt = Person(name: "Matt", imageNamed: "matt")
        matt.sales = 312

        // Feel free to add your own people!
        let reportRenderer = ReportRenderer(persons: [ fred, matt ])

        // Create the report layer
        let reportLayer = ReportLayer(renderer: reportRenderer)
        reportLayer.backgroundColor = UIColor.lightGray.cgColor
        self.reportLayer = reportLayer

        // Put it atop the view
        view.layer.addSublayer(reportLayer)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // DEMO 2 - changed to `reportLayer.heightScale` animation
        let animation = CABasicAnimation(keyPath: "heightScale")
        animation.toValue = 1
        animation.fromValue = 0
        animation.duration = 1
        reportLayer.add(animation, forKey: "grow")
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        var rect = view.bounds
        rect.size.height -= 60 // Leave space for the buttons
        reportLayer.frame = rect
    }

    // DEMO 3 - hook up a button to me
    @IBAction func animateChart(_ sender: UIButton) {
        // DEMO 3a - Basic linear growth
//        let animation = CABasicAnimation(keyPath: "heightScale")
//        anim.toValue = 1
//        anim.fromValue = 0
//        anim.duration = 1
//        reportLayer.add(animation, forKey: "grow")

        // DEMO 3b - Growth with a little overshoot and bounce back. The best part!
        let bounceAnimation = CAKeyframeAnimation(keyPath: "heightScale")
        bounceAnimation.values = [ 1, 0, 1, 1.1, 1 ]
        bounceAnimation.keyTimes = [ 0, 0.3, 0.8, 0.9, 1]
        bounceAnimation.duration = 1
        bounceAnimation.calculationMode = kCAAnimationCubic
        bounceAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        reportLayer.add(bounceAnimation, forKey: "grow")
    }

}
