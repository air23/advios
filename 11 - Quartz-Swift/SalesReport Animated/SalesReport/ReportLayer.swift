//
//  ReportLayer.swift
//  SalesReport
//
//  Created by Zachary Waldowski on 12/2/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

// DEMO 1 - add the entire class so we can pass scale into renderer via custom draw method
class ReportLayer: CALayer {

    private let renderer: ReportRenderer
    dynamic var heightScale: CGFloat = 1

    private func commonInit() {
        isGeometryFlipped = true
        needsDisplayOnBoundsChange = true
    }

    init(renderer: ReportRenderer) {
        self.renderer = renderer
        super.init()
        commonInit()
    }

    override init(layer: Any) {
        let copyFrom = layer as? ReportLayer
        self.renderer = copyFrom?.renderer ?? ReportRenderer(persons: [])

        super.init(layer: layer)
        commonInit()

        if let copyFrom = copyFrom {
            self.heightScale = copyFrom.heightScale
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override class func needsDisplay(forKey key: String) -> Bool {
        switch key {
        case "heightScale":
            return true
        default:
            return super.needsDisplay(forKey: key)
        }
    }

    override func draw(in context: CGContext) {
        renderer.draw(in: context, bounds: bounds, heightRatio: heightScale)
    }

}
