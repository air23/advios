//
//  Person.swift
//  SalesReport
//
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

struct Person {

    let name: String
    let photo: UIImage?

    init(name: String, imageNamed imageName: String) {
        self.name = name
        self.photo = UIImage(named: imageName)
    }

    var sales = 0

}
