//
//  ReportViewController.swift
//  SalesReport
//
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {

    private var reportLayer: CALayer!
    private var reportRenderer: ReportRenderer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up people
        var fred = Person(name: "Fred", imageNamed: "fred")
        fred.sales = 134
        
        var matt = Person(name: "Matt", imageNamed: "matt")
        matt.sales = 312
        
        // Feel free to add your own people!
        reportRenderer = ReportRenderer(persons: [ fred, matt ])
        
        let reportLayer = CALayer()
        reportLayer.backgroundColor = UIColor.lightGray.cgColor
        reportLayer.isGeometryFlipped = true
        reportLayer.needsDisplayOnBoundsChange = true
        reportLayer.delegate = reportRenderer
        self.reportLayer = reportLayer
        
        // Put it atop the view
        view.layer.addSublayer(reportLayer)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var rect = view.bounds
        rect.size.height -= 60 // Leave space for the buttons
        reportLayer.frame = rect
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let animation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.toValue = 0
        animation.fromValue = -view.bounds.width
        reportLayer.add(animation, forKey: "electricSlide")
    }

}
