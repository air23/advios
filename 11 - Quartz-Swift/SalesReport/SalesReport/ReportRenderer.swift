//
//  ReportRenderer.swift
//  SalesReport
//
//  Created by Michael L. Ward on 12/11/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ReportRenderer: NSObject {
    
    private enum Constants {
        // The range of values that are OK
        static let range: Range<CGFloat> = 0 ..< 700
        // The gap between the bars and side margins
        static let padding: CGFloat = 10.4
        // The margins in points
        static let margins = CGSize(width: padding, height: 10)
        // The preferred amount we want to round the bar
        static let cornerRadius: CGFloat = 7
        // The distance from opaque content to draw a shadow
        static let photoShadowOffset: CGSize = CGSize(width: 8, height: -9)
        // The amount to "fuzz" the shadow
        static let photoShadowBlur: CGFloat = 4
    }

    let persons: [Person]
    
    init(persons: [Person]) {
        self.persons = persons
    }
    
    private lazy var backgroundImage = UIImage(named: "flowers")!
    
    private let nameFont = UIFont(name: "Palatino-BoldItalic", size: 39)!
    private let amountAttributes = [
        NSFontAttributeName: UIFont.boldSystemFont(ofSize: 36),
        NSForegroundColorAttributeName: UIColor.white
    ]
    
    private lazy var amountFormatter = MeasurementFormatter()
    private func formattedAmount(_ amount: Double) -> NSAttributedString {
        let measurement = Measurement(value: amount, unit: Unit(symbol: "units"))
        let string = amountFormatter.string(from: measurement)
        return NSAttributedString(string: string, attributes: amountAttributes)
    }
    
    func draw(in context: CGContext, bounds: CGRect) {
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        var strokeRGBA: [CGFloat] = [ 0, 0, 0, 1 ] // black
        var fillRGBA: [CGFloat] = [ 1, 1, 1, 1 ] // white
        var gradientRGBA: [CGFloat] = [
            0.2, 0.2, 0.2, 1.0,  // dark gray
            0.8, 0.8, 0.8, 1.0,  // light gray
        ]
        var gradientLocations: [CGFloat] = [ 0, 1 ]
        
        guard let stroke = CGColor(colorSpace: colorSpace,
                                   components: &strokeRGBA),
              let fill = CGColor(colorSpace: colorSpace,
                                 components: &fillRGBA),
              let gradient = CGGradient(colorSpace: colorSpace,
                                        colorComponents: &gradientRGBA,
                                        locations: &gradientLocations,
                                        count: gradientLocations.count) else { return }
        
        context.setLineWidth(1)
        context.setStrokeColor(stroke)
        context.setFillColor(fill)
        
        let rect = bounds.insetBy(dx: Constants.margins.width, dy: Constants.margins.height)
        let heightIncrement = (rect.maxY - rect.minY)
            / (Constants.range.upperBound - Constants.range.lowerBound)
        
        let interstices = CGFloat(persons.count - 1) * Constants.padding
        let barWidth = (rect.width - interstices) / CGFloat(persons.count)
        
        // Draw the bars
        for (index, minX) in stride(from: rect.minX,
                                    to: rect.maxX,
                                    by: barWidth + Constants.padding).enumerated()
        {
                                        
            let person = persons[index]
            let barRect = CGRect(x: minX,
                                 y: rect.minY,
                                 width: barWidth,
                                 height: heightIncrement * CGFloat(person.sales)).integral
            
            let cornerRadius = min(max((barRect.height - 1) / 2, 0), Constants.cornerRadius)
            let barPath = CGPath(roundedRect: barRect,
                                 cornerWidth: cornerRadius,
                                 cornerHeight: cornerRadius,
                                 transform: nil)
            
            // Fill the bar with flowers
            if let cgImage = backgroundImage.cgImage {
                context.saveGState()
                defer { context.restoreGState() }
                context.addPath(barPath)
                context.clip()
                context.draw(cgImage, in: CGRect(origin: .zero, size: backgroundImage.size))
            }
            
            // Fill path cleared the current path, so add it again
            context.addPath(barPath)
            context.strokePath()
            
            // Draw the sales.
            do {
                UIGraphicsPushContext(context)
                defer { UIGraphicsPopContext() }
                
                context.saveGState()
                defer { context.restoreGState() }
                
                let string = formattedAmount(Double(person.sales))
                
                // Calculate where string should go
                let stringSize = string.size()
                // Negative Y because Y coordinate will be flipped
                let textPoint = CGPoint(x: barRect.maxX - stringSize.width,
                                        y: -barRect.maxY)
                
                // Draw string
                context.scaleBy(x: 1, y: -1)
                string.draw(at: textPoint)
            }
            
            // Draw the name
            let textStart = CGPoint(x: barRect.minX, y: barRect.maxY + Constants.margins.height)
            if let namePath = makePath(forName: person.name, at: textStart) {
                
                do {
                    // Save state since we're changing clipping region
                    context.saveGState()
                    defer { context.restoreGState() }
                    
                    // Clip to it
                    context.addPath(namePath)
                    context.clip()
                    
                    // Set up points for gradient
                    let gradientEnd = CGPoint(x: textStart.x,
                                              y: textStart.y + nameFont.pointSize)
                    
                    // Create and draw gradient
                    context.drawLinearGradient(gradient, start: textStart, end: gradientEnd,
                                               options: [ .drawsBeforeStartLocation, .drawsAfterEndLocation ])
                }
                
                context.addPath(namePath)
                context.strokePath()
            }
                                        
            // Draw the photo
            if let photo = person.photo, let cgPhoto = photo.cgImage {
                context.saveGState()
                defer { context.restoreGState() }
                context.setShadow(offset: Constants.photoShadowOffset,
                                  blur: Constants.photoShadowBlur)
                let photoRect = CGRect(x: barRect.minX,
                                       y: barRect.maxY + nameFont.pointSize + Constants.margins.height,
                                       width: photo.size.width,
                                       height: photo.size.height)
                context.draw(cgPhoto, in: photoRect)
            }
        }
    }
    
    private func makePath(forName name: String,
                          at point: CGPoint) -> CGPath? {
        
        // Get Unicode bytes for the string (what Core Text uses)
        var unichars = Array(name.utf16)
        
        // Get the glyphs
        var glyphs = [CGGlyph](repeating: 0, count: unichars.count)
        guard CTFontGetGlyphsForCharacters(nameFont, &unichars,
                                           &glyphs, unichars.count) else { return nil }
        
        // Get the advances between glyphs
        var advances = [CGSize](repeating: .zero, count: unichars.count)
        CTFontGetAdvancesForGlyphs(nameFont, CTFontOrientation.default,
                                   &glyphs, &advances, unichars.count)
        
        // Create the path that will contain all the letter paths
        let path = CGMutablePath()
        var currentTransform = CGAffineTransform(translationX: point.x, y: point.y)
        for (glyph, advance) in zip(glyphs, advances) {
            // Get the path for a particular glyph
            if let glyphPath = CTFontCreatePathForGlyph(nameFont, glyph, &currentTransform) {
                // Add it to the full path
                path.addPath(glyphPath)
            }
            currentTransform = currentTransform.translatedBy(x: advance.width,
                                                             y: advance.height)
        }
        return path
    }
    
}

extension ReportRenderer: CALayerDelegate {
    
    func draw(_ layer: CALayer, in context: CGContext) {
        draw(in: context, bounds: layer.bounds)
    }
    
}
