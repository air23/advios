
import UIKit

class CatalogListViewController: UIViewController {
    var store: CatalogStore!
    
    @IBOutlet var tableView: UITableView!
    var catalogs = [Catalog]()
    
    private func fetchCatalogs() {
        store.fetchCatalogList() {
            catalogListResult in
            
            switch catalogListResult {
            case let .success(catalogs):
                print("Successfully found \(catalogs.count) catalogs")
                self.catalogs = catalogs
            case let .failure(error):
                print("Error fetching catalogs: \(error)")
            case let .authentication(serverComplaint):
                if let keychain = self.store.keychain,
                    let baseURL = try? CatalogAPI.catalogBaseURL() {
                    keychain.solicitCredentialsFromUserForURL(baseURL, message: serverComplaint) {
                        _ in
                        self.fetchCatalogs()
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if catalogs.isEmpty {
            fetchCatalogs()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCatalog" {
            guard let indexPath = tableView.indexPathForSelectedRow else {
                print("unexpected lack of selected row index path")
                return
            }
            
            let catalogVC = segue.destination as! CatalogViewController
            catalogVC.store = store
            catalogVC.catalog = catalogs[indexPath.row]
        }
    }
}


extension CatalogListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catalogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) 
        
        let catalog = catalogs[indexPath.row]
        cell.textLabel?.text = catalog.name
        
        return cell
    }
}


