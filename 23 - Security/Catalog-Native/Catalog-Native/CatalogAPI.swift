import Foundation

private enum APIMethod : String {
    case baseURL = "/"
    case catalogList = "/api/catalogs"
    case productList = "/api/catalog/{catalogID}"
    case productDetail = "/api/catalog/{catalogID}/product/{productID}"
}

enum CatalogAPIError : Error {
    case malformedURL
    case invalidJSONData
}

private let baseURLString = "https://localhost:5000"


struct CatalogAPI {
    
    private static func catalogURL(method: APIMethod, parameters: [String:String] = [:]) throws ->  URL {
        var path = method.rawValue
        
        for (key, replacement) in parameters {
            path = path.replacingOccurrences(of: "{\(key)}", with: replacement)
        }
        
        let urlString = baseURLString + path
        
        guard let catalogURL = URL(string: urlString) else {
            throw CatalogAPIError.malformedURL
        }
        return catalogURL
    }
    
    static func catalogBaseURL() throws -> URL {
        return try catalogURL(method: .baseURL)
    }

    static func catalogListURL() throws -> URL {
        return try catalogURL(method: .catalogList)
    }
    
    static func productListURL(catalog: Catalog) throws -> URL {
        let parameters = [ "catalogID" : catalog.catalogID ];
        return try catalogURL(method: .productList, parameters: parameters)
    }
    
    static func productDetailURL(product: Product) throws -> URL {
        precondition(product.catalog != nil)
        let parameters = [ "catalogID" : product.catalog!.catalogID,
                           "productID" : product.productID ];
        return try catalogURL(method: .productDetail, parameters: parameters)
    }

    
    private static func catalogFromJSONObject(_ json: [String : AnyObject]) -> Catalog? {
        guard
            let name = json["name"] as? String,
            let catalogID = json["id"] as? String else {
                return nil
        }
        return Catalog(name: name, catalogID: catalogID)
    }
    
    private static func productFromJSONObject(_ json: [String : AnyObject]) -> Product? {
        guard
            let name = json["name"] as? String,
            let productID = json["id"] as? String,
            let price = json["price"] as? Int else {
                return nil
        }
        
        let product = Product(catalog: nil, name: name, productID: productID, price: price)
        
        product.blurbs = json["blurbs"] as? [String]
        product.specs = json["specs"] as? [String]
        
        return product
    }
    
    static func catalogListFromJSONData(_ data: Data) -> CatalogListResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as AnyObject
            
            if let catalogsArray = jsonObject["response"] as? [[String : AnyObject]] {
                var finalCatalogs = [Catalog]()
                for catalogJSON in catalogsArray {
                    if let catalog = catalogFromJSONObject(catalogJSON) {
                        finalCatalogs.append(catalog)
                    }
                }
                
                if finalCatalogs.isEmpty && !catalogsArray.isEmpty {
                    // Couldn't parse any of the catalogs.  Maybe the JSON payload has changed
                    return .failure(CatalogAPIError.invalidJSONData)
                }
                else {
                    return .success(finalCatalogs)
                }
            }
            
            // Data isn't nil, and we were able to parase the json, but couldn't find the
            // array of catalogs. Maybe the JSON payload has changed.
            return .failure(CatalogAPIError.invalidJSONData)
        } catch {
            print("Error parsing JSON: \(error)")
            return .failure(error)
        }
    }
    
    static func productListFromJSONData(_ data: Data) -> ProductListResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as AnyObject
            
            if let productsArray = jsonObject["response"] as? [[String : AnyObject]] {
                var finalProducts = [Product]()
                for productJSON in productsArray {
                    if let product = productFromJSONObject(productJSON) {
                        finalProducts.append(product)
                    }
                }
                
                if finalProducts.isEmpty && !productsArray.isEmpty {
                    // Couldn't parse any of the products.  Maybe the JSON payload has changed
                    return .failure(CatalogAPIError.invalidJSONData)
                }
                else {
                    return .success(finalProducts)
                }
            }
            
            // Data isn't nil, and we were able to parase the json, but couldn't find the
            // array of products. Maybe the JSON payload has changed.
            return .failure(CatalogAPIError.invalidJSONData)
        } catch {
            print("Error parsing JSON: \(error)")
            return .failure(error)
        }
    }
    
    static func productDetailFromJSONData(_ data: Data) -> ProductResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as AnyObject
            
            if let productJSON = jsonObject["response"] as? [String : AnyObject] {
                guard let product = productFromJSONObject(productJSON) else {
                    return .failure(CatalogAPIError.invalidJSONData)
                }
                return .success(product)
            }
            
            // Data isn't nil, and we were able to parase the json, but couldn't find the
            // array of products. Maybe the JSON payload has changed.
            return .failure(CatalogAPIError.invalidJSONData)
        } catch {
            print("Error parsing JSON: \(error)")
            return .failure(error)
        }
    }
}


