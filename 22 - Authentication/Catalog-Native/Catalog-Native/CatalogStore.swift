import Foundation

enum CatalogStoreError : Error {
    case badHTTPResponse
}

enum CatalogListResult {
    case success([Catalog])
    case failure(Error)
    case authentication(String)
}

enum ProductListResult {
    case success([Product])
    case failure(Error)
}

enum ProductResult {
    case success(Product)
    case failure(Error)
}



class CatalogStore: NSObject {
    var keychain: Keychain?
    
    init(keychain: Keychain?) {
        self.keychain = keychain
    }
    
    private lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()
    
    func processCatalogListRequest(data: Data?, error: Error?) -> CatalogListResult {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return CatalogAPI.catalogListFromJSONData(jsonData)
    }
    
    func fetchCatalogList(_ completion: @escaping (CatalogListResult) -> Void) {
        do {
            let url = try CatalogAPI.catalogListURL()
            let request = URLRequest(url: url as URL)
            
            let task = session.dataTask(with: request) {
                data, response, error in
                
                var result: CatalogListResult
                
                defer {
                    OperationQueue.main.addOperation {
                        completion(result)
                    }
                }
                
                guard let response = response else {
                    result = .failure(error!)
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    result = .failure(CatalogStoreError.badHTTPResponse)
                    return
                }
                
                switch httpResponse.statusCode {
                case 200:
                    result = self.processCatalogListRequest(data: data, error: error)
                    
                case 401:
                    let serverMessage = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as? String
                    result = .authentication(serverMessage ?? "Authentication Error")
                    
                default:
                    result = .failure(CatalogStoreError.badHTTPResponse)
                }
            }
            task.resume()
            
        } catch CatalogAPIError.malformedURL {
            print("Catalog API could not build the URL we asked for")
        } catch {
            print("unexpected error: \(error)")
        }
    }
    
    
    func processProductListRequest(data: Data?, error: Error?) -> ProductListResult {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return CatalogAPI.productListFromJSONData(jsonData)
    }
    
    
    func fetchProductList(_ catalog: Catalog, completion: @escaping (ProductListResult) -> Void) {
        do {
            let url = try CatalogAPI.productListURL(catalog: catalog)
            
            let request = URLRequest(url: url as URL)
            let task = session.dataTask(with: request) {
                data, response, error in
                
                var result: ProductListResult
                
                defer {
                    OperationQueue.main.addOperation {
                        completion(result)
                    }
                }
                
                guard let response = response else {
                    result = .failure(error!)
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    result = .failure(CatalogStoreError.badHTTPResponse)
                    return
                }
                
                switch httpResponse.statusCode {
                case 200:
                    result = self.processProductListRequest(data: data, error: error)
                    
                    if case let .success(products) = result {
                        // set the back-reference to the catalog.
                        products.forEach { $0.catalog = catalog }
                        catalog.products = products
                    }
                default:
                    result = .failure(CatalogStoreError.badHTTPResponse)
                }
            }
            task.resume()
            
        } catch CatalogAPIError.malformedURL {
            print("Catalog API could not build the URL we asked for")
        } catch {
            print("unexpected error")
        }
    }
    
    func processProductDetailRequest(data: Data?, error: Error?) -> ProductResult {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return CatalogAPI.productDetailFromJSONData(jsonData)
    }
    
    
    func fetchProductDetail(_ product: Product, completion: @escaping (ProductResult) -> Void) {
        do {
            let url = try CatalogAPI.productDetailURL(product: product)
            
            let request = URLRequest(url: url as URL)
            let task = session.dataTask(with: request) {
                data, response, error in
                
                var result: ProductResult
                
                guard response != nil else {
                    result = .failure(error!)
                    completion(result)
                    return
                }
                
                let httpResponse = response as! HTTPURLResponse
                
                if httpResponse.statusCode == 200 {
                    result = self.processProductDetailRequest(data: data, error: error)
                    
                    switch result {
                    case let .success(newProduct):
                        product.blurbs = newProduct.blurbs
                        product.specs = newProduct.specs
                        result = .success(product)  // we mutated the existing product, so send that along
                    default:
                        break
                    }
                } else {
                    result = .failure(CatalogStoreError.badHTTPResponse)
                }
                
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
            task.resume()
            
        } catch CatalogAPIError.malformedURL {
            print("Catalog API could not build the URL we asked for")
        } catch {
            print("unexpected error")
        }
    }
}

extension CatalogStore : URLSessionTaskDelegate {
    func urlSession (_ session: URLSession, task: URLSessionTask,
                     didReceive challenge: URLAuthenticationChallenge,
                     completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.previousFailureCount > 0 {
            completionHandler(.rejectProtectionSpace, nil)
        } else {
            if let credentials = keychain?.credentialsForURL(try! CatalogAPI.catalogBaseURL()) {
                let urlCredential = URLCredential(user: credentials.username, password: credentials.password, persistence: .forSession)
                completionHandler(.useCredential, urlCredential)
            } else {
                completionHandler(.performDefaultHandling, nil)
            }
        }
    }
}
