import UIKit

class Keychain : NSObject {
    
    fileprivate var solicitationServiceURL: URL?
    fileprivate var solicitationCompletion: ((Bool) -> Void)?
    
    fileprivate let usernameDefaultsKey = "Keychain.username"
    
    func credentialsForURL(_ url: URL) -> (username: String, password: String)? {
        let defs = UserDefaults.standard
        let defsUsername = defs.object(forKey: usernameDefaultsKey) as? String
        
        if let username = defsUsername, let password = passwordForUsername(username, forServiceAtURL: url) {
            return (username: username, password: password)
        }
        return nil
    }

    private func passwordForUsername (_ username: String, forServiceAtURL url: URL?) -> String? {
        let keychainQuery: [NSString : Any] = [
            kSecClass : kSecClassInternetPassword,
            kSecAttrAccount : username,
            kSecAttrServer : url?.host ?? "",
            kSecAttrProtocol : url?.scheme ?? "",
            
            kSecReturnData : kCFBooleanTrue
        ]
        
        var cfTypeResult: AnyObject?
        let status = SecItemCopyMatching(keychainQuery as CFDictionary, &cfTypeResult)
        
        var password: String? = nil
        
        switch status {
        case noErr:
            guard let retrievedData = cfTypeResult as? Data else {
                print("unexpected return type from SecItemCopyMatching");
                break
            }
            password = NSString(data: retrievedData, encoding: String.Encoding.utf8.rawValue) as? String
        case errSecItemNotFound:
            print("could not find item")
        case errSecParam:
            print("parameter error looking up keychain item")
        default:
            print("other error looking up keychain item: \(status)")
        }
        return password
    }
    
    
    func solicitCredentialsFromUserForURL(_ url: URL?, message: String?, completion: @escaping (Bool) -> Void) {
        let hostname = url?.host ?? "the catalog server"
        solicitationCompletion = completion
        solicitationServiceURL = url
        
        let alert = UIAlertView(title: "Authentication needed for \(hostname)",
            message: message ?? "", delegate: self,
            cancelButtonTitle: "Cancel", otherButtonTitles: "OK")
        alert.alertViewStyle = .loginAndPasswordInput
        alert.show()
    }
    
    
    fileprivate func setPassword(_ password: String?, username: String?, forServiceAtURL url: URL) {
        guard let username = username else {
            return
        }
        guard let password = password else {
            removeKeychainItemForUsername(username, forServiceAtURL: url)
            return
        }
        addKeychainItemForUsername(username, password: password, forServiceAtURL: url)
    }
    
    private func removeKeychainItemForUsername(_ username: String?, forServiceAtURL url: URL) {
        let keychainQuery: [NSString : Any] = [
            kSecClass : kSecClassInternetPassword,
            kSecAttrAccount : username ?? "",
            kSecAttrServer : url.host ?? "",
            kSecAttrProtocol : url.scheme ?? ""
        ]
        
        let status = SecItemDelete(keychainQuery as CFDictionary)
        
        switch status {
        case noErr:
            break
        case errSecParam:
            print("Parameter error removing keychain item")
        case errSecItemNotFound:
            print("Item being removed doesn't exist. (This isn't a fatal error)")
        default:
            print("other error removing keychain item: \(status)")
        }
    }
    
    private func addKeychainItemForUsername(_ username: String, password: String, forServiceAtURL url: URL) {
        
        guard let passwordAsData = password.data(using: String.Encoding.utf8, allowLossyConversion: false) else {
            print("Could not convert password into UTF-8")
            return
        }
        
        let keychainQuery: [NSString : Any] = [
            kSecClass : kSecClassInternetPassword,
            kSecAttrAccount : username,
            kSecAttrServer : url.host ?? "",
            kSecAttrProtocol : url.scheme ?? "",
            kSecAttrAccessible : kSecAttrAccessibleWhenUnlocked,
            kSecValueData : passwordAsData
        ]
        
        removeKeychainItemForUsername(username, forServiceAtURL: url)
        
        let status = SecItemAdd(keychainQuery as CFDictionary, nil)
        
        switch status {
        case noErr:
            print("successfully added keychain item")
        case errSecParam:
            print("parameter error adding keychain item")
        case errSecDuplicateItem:
            print("Keychain item already exists")
        default:
            print("other error adding keychain item: \(status)")
        }
    }
}


extension Keychain : UIAlertViewDelegate {
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        precondition(solicitationCompletion != nil)
        
        let CancelButtonIndex = 0
        let OKButtonIndex = 1
        let LoginTextfieldIndex = 0
        let PasswordTextfieldIndex = 1
        
        switch (buttonIndex) {
        case CancelButtonIndex:
            solicitationCompletion!(false)
        case OKButtonIndex:
            let username = alertView.textField(at: LoginTextfieldIndex)?.text
            let password = alertView.textField(at: PasswordTextfieldIndex)?.text
            
            let defs = UserDefaults.standard
            defs.set(username, forKey: usernameDefaultsKey)
            defs.synchronize()
            
            setPassword(password, username: username, forServiceAtURL: solicitationServiceURL!)
            
            solicitationCompletion!(true)
        default:
            print("Unexpected button 'clicked': \(buttonIndex)")
            solicitationCompletion!(false)
        }
    }
}


