import UIKit

class CatalogViewController : UIViewController {
    
    var store: CatalogStore!
    var catalog: Catalog? {
        didSet {
            title = catalog?.name ?? "Catalog"
        }
    }
    
    @IBOutlet var tableView: UITableView!

    var products = [Product]()
    
    private func fetchProducts() {
        store.fetchProductList(catalog!) {
            productResult in
            
            switch productResult {
            case let .success(products):
                print("Successfully found \(products.count) products")
                self.products = products
            case let .failure(error):
                print("Error fetching products: \(error)")
            }
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if catalog != nil && products.isEmpty {
            fetchProducts()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProductDetail" {
            guard let indexPath = tableView.indexPathForSelectedRow else {
                print("unexpected lack of selected row index path")
                return
            }
            
            let detailVC = segue.destination as! ProductDetailViewController
            detailVC.store = store
            detailVC.product = products[indexPath.row]
        }
    }
}

extension CatalogViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) 

        let product = catalog?.products[indexPath.row]
        cell.textLabel?.text = product?.name

        return cell
    }
}
